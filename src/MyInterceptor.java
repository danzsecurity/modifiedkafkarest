import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONObject;

public class MyInterceptor<K, V> implements ProducerInterceptor<K, V> {
    private DateTime interceptorNewTimestamp, interceptorLastTimestamp;
    private String recordNewTimestamp, recordLastTimestamp;
    private Period interceptorDelay, recordDelay, interceptorRecordDelay;
    private String uuid, size, jsonString;
    private LinkedHashMap<String, List<String>> map;
    private KafkaProducer<String, String> infoProducer;

    public MyInterceptor() {
        map = new LinkedHashMap<>();

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        this.infoProducer = new KafkaProducer<>(props);

        interceptorLastTimestamp = null;
        recordLastTimestamp = null;
    }

    @Override
    public void configure(Map<String, ?> configs) {
    }

    @Override
    public ProducerRecord<K, V> onSend(ProducerRecord<K, V> record) {
        // Get record uuid and size
        uuid = getValue(record.value().toString(), "UUID", "TIMESTAMP");
        size = String.valueOf(record.value().toString().getBytes().length);

        // Get old timestamps
        if (map.containsKey(uuid)) {
            interceptorLastTimestamp = new DateTime(map.get(uuid).get(0));
            recordLastTimestamp = map.get(uuid).get(1);
        } else {
            interceptorLastTimestamp = null;
            recordLastTimestamp = null;
        }

        // Get interceptor timestamp and delay between intercepted timestamp
        interceptorNewTimestamp = getInterceptorTimestamp();

        if (interceptorLastTimestamp != null) {
            interceptorDelay = getDelay(interceptorLastTimestamp, interceptorNewTimestamp);
        } else {
            interceptorDelay = getDelay(interceptorNewTimestamp, interceptorNewTimestamp);
        }

        // Get record timestamp and delay between record timestamp
        recordNewTimestamp = getValue(record.value().toString(), "TIMESTAMP", "FECHA");

        if (recordLastTimestamp != null) {
            recordDelay = getDelay(new DateTime(recordLastTimestamp), new DateTime(recordNewTimestamp));
        } else {
            recordDelay = getDelay(new DateTime(recordNewTimestamp), new DateTime(recordNewTimestamp));
        }

        // Save current timestamps as old timestamps
        map.put(uuid, Arrays.asList(interceptorNewTimestamp.toString(), recordNewTimestamp));

        // Get delay between intercepted and record timestamp
        interceptorRecordDelay = getDelay(new DateTime(recordNewTimestamp), interceptorNewTimestamp);
        if (interceptorLastTimestamp == null) {
            interceptorLastTimestamp = interceptorNewTimestamp;
            recordLastTimestamp = recordNewTimestamp;
        }

        jsonString = prepareJson(interceptorNewTimestamp.toString(), interceptorLastTimestamp.toString(),
                interceptorDelay.toString(), recordNewTimestamp, recordLastTimestamp, recordDelay.toString(),
                interceptorRecordDelay.toString(), uuid, size);

        // Produce the record with the metrics
        ProducerRecord<String, String> infoRecord = new ProducerRecord<>("recordInfo", "INFO",
                jsonString);
        infoProducer.send(infoRecord);

        return record;
    }

    // Puts all the variables in a JSON format and returns it
    private String prepareJson(String interceptorNewTimestamp, String interceptorLastTimestamp, String interceptorDelay,
                               String recordNewTimestamp, String recordLastTimestamp, String recordDelay, String interceptorRecordDelay,
                               String uuid, String size) {

        JSONObject obj = new JSONObject();
        obj.put("interceptorNewTimestamp", interceptorNewTimestamp);
        obj.put("interceptorLastTimestamp", interceptorLastTimestamp);
        obj.put("interceptorDelay", interceptorDelay);
        obj.put("recordNewTimestamp", recordNewTimestamp);
        obj.put("recordLastTimestamp", recordLastTimestamp);
        obj.put("recordDelay", recordDelay);
        obj.put("interceptorRecordDelay", interceptorRecordDelay);
        obj.put("uuid", uuid);
        obj.put("size", size);

        return obj.toString();
    }

    // Returns the delay between two DateTimes
    private Period getDelay(DateTime dateTime, DateTime dateTime2) {
        return new Period(new DateTime(dateTime), dateTime2, PeriodType.millis());
    }

    // Returns the current timestamp
    private DateTime getInterceptorTimestamp() {
        return new DateTime().withZone(DateTimeZone.forID("Europe/Madrid"));
    }

    // Returns a value from the original record, after establishing the column
    // you want and the one next to it
    private static String getValue(String record, String column, String nextColumn) {
        int min, max;
        min = record.indexOf(column) + column.length() + 1;
        max = record.indexOf(nextColumn) - 2;
        record = record.substring(min, max);

        return record;
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
    }

    @Override
    public void close() {
        infoProducer.close();
    }

}
